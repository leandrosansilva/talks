module example

go 1.18

require gitlab.com/lightmeter/controlcenter v0.0.0-20220508085653-50fb4416ab0b

require (
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/rs/zerolog v1.20.0 // indirect
)

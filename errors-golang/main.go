package main

import (
	"io"
	"os"

	"gitlab.com/lightmeter/controlcenter/util/errorutil"
)

type diff struct {
}

func computeDiff(from, to io.Reader) (*diff, error) {
	// Compute the diff between two files here...
	return &diff{}, nil
}

func openFileV1(path string) (io.ReadCloser, error) {
	return os.Open(path)
}

func computeDiffFromPathsV1(fromPath, toPath string) (*diff, error) {
	from, err := openFileV1(fromPath)
	if err != nil {
		return nil, err
	}

	defer from.Close()

	to, err := openFileV1(toPath)
	if err != nil {
		return nil, err
	}

	defer to.Close()

	return computeDiff(from, to)
}

func computeDiffFromPathsV2(fromPath, toPath string) (diff *diff, err error) {
	from, err := openFileV1(fromPath)
	if err != nil {
		return nil, err
	}

	defer errorutil.UpdateErrorFromCloser(from, &err)

	to, err := openFileV1(toPath)
	if err != nil {
		return nil, err
	}

	defer errorutil.UpdateErrorFromCloser(to, &err)

	return computeDiff(from, to)
}

func openFileV2(path string) (io.ReadCloser, func() error, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, nil, err
	}

	return f, func() error { return f.Close() }, nil
}

func computeDiffFromPathsV3(fromPath, toPath string) (diff *diff, err error) {
	from, releaseFrom, err := openFileV2(fromPath)
	if err != nil {
		return nil, err
	}

	defer errorutil.UpdateErrorFromCall(releaseFrom, &err)

	to, releaseTo, err := openFileV2(toPath)
	if err != nil {
		return nil, err
	}

	defer errorutil.UpdateErrorFromCall(releaseTo, &err)

	return computeDiff(from, to)
}

func main() {
	errorutil.Wrap(nil)
}

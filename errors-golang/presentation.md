# Never forget to handle errors, rediscovered the 1000th time 

## Leandro Santiago (Lead Software Engineer at Lightmeter)
### June 2022

---

# Let's suppose we have some code which is able to compute the difference between two readers

```golang
type diff struct { // some magic here...  }

func computeDiff(localFile, remoteFile io.Reader) (*diff, error)
```

---

# We can then use it with real files

```golang
func openFileV1(path string) (io.ReadCloser, error) {
  return os.Open(path)
}

func computeDiffFromPathsV1(localPath, remotePath string) (*diff, error) {
  localFile, err := openFileV1(localPath)
  if err != nil {
    return nil, err
  }

  defer localFile.Close()

  remoteFile, err := openFileV1(remotePath)
  if err != nil {
    return nil, err
  }

  defer remoteFile.Close()

  return computeDiff(localFile, remoteFile)
}
```

---

# We can then use it with real files

```golang
func openFileV1(path string) (io.ReadCloser, error) {
  return os.Open(path)
}

func computeDiffFromPathsV1(localPath, remotePath string) (*diff, error) {
  localFile, err := openFileV1(localPath)
  if err != nil {
    return nil, err
  }

  defer localFile.Close() // 1. No error handling

  remoteFile, err := openFileV1(remotePath)
  if err != nil {
    return nil, err
  }

  defer remoteFile.Close() // 1. No error handling

  return computeDiff(localFile, remoteFile)
}
```

---

# We can then use it with real files

```golang
func openFileV1(path string) (io.ReadCloser, error) {
  return os.Open(path)
}

func computeDiffFromPathsV1(localPath, remotePath string) (*diff, error) {
  localFile, err := openFileV1(localPath)
  if err != nil {
    return nil, err
  }

  defer localFile.Close() // 1. No error handling; 2. I can forget to close it

  remoteFile, err := openFileV1(remotePath)
  if err != nil {
    return nil, err
  }

  defer remoteFile.Close() // 1. No error handling; 2. I can forget to close it

  return computeDiff(localFile, remoteFile)
}
```

---

# Fix 1st issue: Handle error on defer

```golang
func computeDiffFromPathsV2(localPath, remotePath string) (diff *diff, err error) {
  localFile, err := openFileV1(localPath)
  if err != nil {
    return nil, err
  }

  defer errorutil.UpdateErrorFromCloser(localFile, &err)

  remoteFile, err := openFileV1(remotePath)
  if err != nil {
    return nil, err
  }

  defer errorutil.UpdateErrorFromCloser(remoteFile, &err)

  return computeDiff(localFile, remoteFile)
}

```

---

# Under the hood: Multierror

Based on hashicorp's `go-multierror`:

![screenshot](multierror.png)

---

# Implementation, just for reference

Not a standalone library; still part of [Lightmeter ControlCenter](https://gitlab.com/lightmeter/controlcenter). For reference, simplified implementation:

```golang
func UpdateErrorFromCall(f func() error, err *error) { AssertNotNil(err)
  cErr := f()
  if cErr == nil {
    return
  }
  
  if *err == nil {
    *err = Wrap(cErr)
    return
  }
  
  *err = multierror.Append(*err, Wrap(cErr))
}

func UpdateErrorFromCloser(closer io.Closer, err *error) {
  UpdateErrorFromCall(closer.Close, err)
}
```

---

# Fixing 2nd problem: force the compiler to check it

When acquiring a resource (here, a file), we also return to the caller a function responsible for releasing such resource.
Such function must be called, otherwise the compile will complain that it's not being used.

```golang
func openFileV2(path string) (io.ReadCloser, func() error, error) {
  f, err := os.Open(path)
  if err != nil {
    return nil, nil, err
  }

  return f, func() error { return f.Close() }, nil
}

```

---

# Fixing 2nd problem: force the compiler to check it

```golang
func computeDiffFromPathsV3(localPath, remotePath string) (diff *diff, err error) {
  localFile, releaseLocalFile, err := openFileV2(localPath)
  if err != nil {
    return nil, err
  }

  defer errorutil.UpdateErrorFromCall(releaseLocalFile, &err)

  remoteFile, releaseRemoteFile, err := openFileV2(remotePath)
  if err != nil {
    return nil, err
  }

  defer errorutil.UpdateErrorFromCall(releaseRemoteFile, &err)

  return computeDiff(localFile, remoteFile)
}

```

---

# That's it folks

Leandro Santiago

- <leandro@lightmeter.io>
- <leandro@setefaces.org>
- [LinkedIn Profile](https://www.linkedin.com/in/leandrosansilva/)

## We are hiring; please talk to me :-)
- [Principal Network Engineer](https://www.ycombinator.com/companies/lightmeter/jobs/PB5qMli-principal-network-engineer)
